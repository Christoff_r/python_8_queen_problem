import numpy as np

# Can we place a queen here
def valid_position(b, y, x):
    # Loop over the linst
    for i in range(len(b)):
        for j in range(len(b)):
            ## If wee find a queen
            if b[i][j] == 1:
                ## Check if the quuen is placed diagonoly from our position
                if (i - y) == (j - x):
                    return False
                ## Check if the queen is placed at the same row or collunm as our position
                if (i == y or j == x):
                    return False
    
    return True  

## Try to solve the 8 quen problem
def try_to_solve(b):
    # Loop over the linst
    for x in range(len(b)):
        for y in range(len(b)):
            # If we find a empty spot
            if b[x][y] == 0:
                # Check if the position is valid 
                if valid_position(b, x, y):
                    # Try placeing a queen
                    b[x][y] = 1
                    try_to_solve(b)
                    # If we get to here, we got through the boeard
                    # Check if the sum of the board is 8 (meaning we placed all 8 queens)
                    if np.sum(b) == 8:
                        return b
                    # Else set the spot to empty again, and move to next spot on the board
                    b[x][y] = 0