# 8 Queens Problem 

![banner](https://i.gifer.com/79Pw.gif)

![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen)
![language](https://img.shields.io/badge/language-python-yellow)
![hello-world](https://img.shields.io/badge/Hello-world-green)

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)


## Background

The 8 Queen Problem is the problem of placing 8 chess queens on a 8*8 sized chessboard, so that no two queens can take one another, thus no two queens can share the same row, column or diagonal. An eksample can be seen below.

This program uses [depth-first search](https://en.wikipedia.org/wiki/Depth-first_search) to find a solution to this problem.

![00](https://miro.medium.com/max/457/1*SVCP2lIp1jfzJuQn_QUeVg.png)


The purpose pf this repository is to introduce me to the basics of python.

## Install
Below is a short description on how to install the program.

### Dependencies
The program uses numpy, and thus numpy needs to be installed.

Install using pip

```
pip install numpy
```

Install using conda

```
conda install numpy
```

### Cloneing from gitlab

Navigate to were you want the repository to be. Open the `CLI` a type the following.

```
git clone git@gitlab.com:Christoff_r/python_8_queen_problem.git
```

## Usage
In the repository open the `CLI` and type the following. 

```
python3 main.py
```

If the program finds a solution it will be printed to the consol.

## Contributing

If you have any tips, sugestions or things like that feel free to send me a message :speech_balloon:, although I might me slow to respond :sweat_smile:

## License

`UNLICENSED`